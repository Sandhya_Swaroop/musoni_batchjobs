import requests
from requests.auth import HTTPBasicAuth
import pandas as pd
import json
import datetime
import time
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import psycopg2
from io import StringIO
import csv
import numpy as np
import logging
import warnings
warnings.filterwarnings("ignore")  
# config the logging behavior
logging.basicConfig(filename='AL_update_MusoniIms.log',filemode='a',level=logging.DEBUG)


date = datetime.datetime.now().date()
print(date)

logging.info("------- {} --------".format(date))

error_log = []

def api_call(relativeurl,params,method,data):
    headers = {'X-Fineract-Platform-TenantId': 'sokowatch'}
    base_url =  "https://demo.irl.musoniservices.com:8443/api/v1"
    response = requests.request(method,base_url+relativeurl,headers=headers,verify=False,auth=HTTPBasicAuth('sokowatch', 'sokowatch123'),params = params,data=data)
    if response.status_code  == 200:

        return "success",response.json()
    else:
        logging.debug('API call to the relative URL {} returned a status code of {}'.format(relativeurl,response.status_code))
        error_log.append(response.status_code)
        return "failed",{}
    
# update available credit limit

def create_payload(client_id,client_data):
    return json.dumps({"Available_Credi10":client_data[client_data['client_id']==client_id]['Available_Limit'].values[0],
                       "locale":"en_GB"})
    
def get_client_details():
     func_status,client_det  = api_call("/data-export/prepared/addToQueue",{"tenantIdentifier":"sokowatch","exportId":5,"fileFormat":"csv"},"GET",{})
     response = api_call("/data-export/prepared/{}/attachment/direct-link".format(client_det),{},"GET",{})
     client_data = pd.read_csv(response[1]['value'],delimiter=';')

     func_status,all_client_det = api_call("/clients",{},"GET",{})
     if func_status == "success":
         try:
             client_details = pd.json_normalize(all_client_det['pageItems'])
             client_details  = pd.merge(client_data,client_details[['id','externalId']],how='left',right_on='id',left_on='Client Id')
            
         except Exception as e:
              error_log.append(str(e))

              logging.debug("Error -- {} captured while merging custom client details and all client data Func - get_client_details".format(e))
     
     return client_details
    
def update_available_limit():
    
    print("Fetching Loan details from Musoni")
    func_status,loan_data = api_call("/loans",{},"GET",{})
    
    if func_status == "success":
        try:
            loan_data = pd.json_normalize(loan_data['pageItems'])
      
            loan_data['Outstanding_amount'] = loan_data["summary.totalOutstanding"] .apply(lambda x: 0 if x < 0  else x)
            loans_per_client = loan_data.groupby("clientId",as_index=False)["Outstanding_amount"].sum()
        except Exception as e:
           error_log.append(str(e))

           logging.debug("Error --- {} captured while calculating the outstanding amount per client".format(e))
           
    else:
        pass
              
    # For each loan get custom client details to update their credit limit
    logging.info("Fetching client details")
    print("Fetching client details")
    client_data = get_client_details()
    
    # Calculate available limit
    logging.info("Calculating Available Limit")
    print("Calculating Available Limit")
    client_limit = pd.merge(client_data[['Client Id','External Id','Client Details - Credit Limit', 'Client Details - Available Credit Limit',]],loans_per_client,how='left',right_on="clientId",left_on = 'Client Id')
    client_limit['Client Details - Credit Limit'] = client_limit['Client Details - Credit Limit'].astype(float).fillna(0)
    client_limit['Client Details - Credit Limit'] = client_limit['Client Details - Credit Limit'].astype(float).fillna(0)

    client_limit['AvailableCreditLimit'] = client_limit['Client Details - Credit Limit'] - client_limit['Outstanding_amount']
    client_limit['Available_Limit'] = client_limit.apply(lambda x:0 if x["Outstanding_amount"] > 0 else x['AvailableCreditLimit'],axis=1) 

    # Update available limit to customers whose credit limit has been modified
    client_limit['is_limit_changed'] = np.where(client_limit['Client Details - Available Credit Limit'] == client_limit['Available_Limit'],False,True)
    client_ids = list(client_limit['Client Id'][client_limit['is_limit_changed']==True].unique())
    logging.info("Updating Available Client Limit in Musoni")
    print("Client Ids to update {}".format(str(client_ids)))
    for client_id in client_ids:
        func_status,response = api_call("/datatables/ml_client_details/"+str(client_id),{"tenantIdentifier":"sokowatch","apptable":"m_client"},"PUT",data = create_payload(client_id,client_limit))
        logging.info(response)
    return client_limit,client_ids

def update_credit_limit_toIMS(client_limit,client_ids):
    conn = psycopg2.connect(
       database="erp_dev", user='khBw2W', password='QXa5JggXZ5KFPuh2g95W', host='206.189.125.48', port= '5432'
    )
    
    cursor = conn.cursor()
    
    cursor.execute("CREATE TEMP TABLE musoni_ims_limit_update (customer_id varchar(70),musoni_client_id varchar(30),credit_limit float,available_limit float);")
    
    cursor.execute("CREATE INDEX customer_id_idx on musoni_ims_limit_update(customer_id);")
    
    temp = client_limit[["externalId","client_id","Available_Credi10","Available_Limit"]]
    
    temp = temp[temp['Client Id'].isin(client_ids)]
    temp['Client Id'] = temp['Client Id'].astype('str')
    
    sio = StringIO()
    writer = csv.writer(sio)
    writer.writerows(temp.values)
    sio.seek(0)
    
    with conn.cursor() as c:
        c.copy_from(
            file=sio,
            table="musoni_ims_limit_update",
            columns=[
                "customer_id",
                "musoni_client_id",
                "credit_limit",
                "available_limit"
    
            ],
            sep=","
        )
        conn.commit()
        
    start_time = datetime.datetime.now()
    update_sql = """
                UPDATE customers  SET credit_limit = t.available_limit 
                FROM musoni_ims_limit_update t WHERE id = t.customer_id 
            """    
    logging.info("Update customers table - Execution time {}".format(datetime.datetime.now() - start_time))
    
    cursor.execute(update_sql)
    conn.commit()
    
    conn.close()

if __name__=="__main__":
    print("--------Updating Available Credit Limit-------")
    client_limit,client_ids = update_available_limit()
    print("--------Updating Credit Limit column to the customers table in IMS--------")
    try:
        update_credit_limit_toIMS(client_limit,client_ids)   
    except Exception as e:
        error_log.append(str(e))

        logging.debug("Error {} caught while updating credit limit data to customers table".format(e) )
    if  len(error_log)>=1:
        logging.info("Script ran with the following errors -- {}".format(" ".join(error_log)))
    else:
        logging.info("Script ran successfully")
