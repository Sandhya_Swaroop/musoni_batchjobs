import requests
from requests.auth import HTTPBasicAuth
import pandas as pd
import json
import datetime
import time
import psycopg2
from io import StringIO
import csv
import numpy as np
import logging
import os
import sys
import warnings
warnings.filterwarnings("ignore")  

# config the logging behavior
logging.basicConfig(level=logging.DEBUG,
                    handlers=[logging.StreamHandler(sys.stdout),logging.FileHandler('AL_update_MusoniIms.log')])


DB_CONNECTION_HOST = os.getenv('DB_CONNECTION_HOST','206.189.125.48')
DB_CONNECTION_DB = os.getenv('DB_CONNECTION_DB', 'erp_dev')
DB_CONNECTION_PORT = os.getenv('DB_CONNECTION_PORT','5432')
DB_CONNECTION_USERNAME = os.getenv('DB_CONNECTION_USERNAME','khBw2W')
DB_CONNECTION_PASSWORD = os.getenv('DB_CONNECTION_PASSWORD','QXa5JggXZ5KFPuh2g95W')

MUSONI_BASE_URL = os.getenv('MUSONI_BASE_URL','https://demo.irl.musoniservices.com:8443/api/v1')
MUSONI_API_KEY = os.getenv('MUSONI_API_KEY', "4ihoKzyAQa7HX1JLfdcmr3aiTxbDpbliaDXth6SN")
MUSONI_HEADERS = {
    'X-Fineract-Platform-TenantId': 'sokowatch',
    'x-api-key': MUSONI_API_KEY
}

MUSONI_USERNAME = os.getenv('MUSONI_USERNAME', "sokowatch")
MUSONI_PWD = os.getenv('MUSONI_PWD',"sokowatch12")



loandet_url = "/loans"
# Fetch custom client details for all customers at once using data export api
addto_queue_url = "/data-export/prepared/addToQueue" # returns report id
reportdet_url  = "/data-export/prepared/{}/attachment/direct-link"
clientdet_url = "/client"
update_clientdet_url = "/datatables/ml_client_details/{}"

date = datetime.datetime.now()

logging.info("------- %s --------",str(date))


error_log = []


def api_call(method,relativeurl,params=None,data=None):
    url =  MUSONI_BASE_URL+relativeurl
    auth = HTTPBasicAuth(MUSONI_USERNAME , MUSONI_PWD)
    response = requests.request(method,url,headers = MUSONI_HEADERS,auth = auth,params  = params ,data = data)
    logging.info("Received code %s as response to the api call %s",response.status_code,url)
    if response.status_code not in(200,201):
        raise Exception('API call to the relative URL "{}" returned a status code of {} due to "{}"'.format(relativeurl,response.status_code,response))
    return response.json()
    
# update available credit limit

def create_payload(client_id,client_data):
    return {"Available_Credi10":client_data[client_data['Client Id']==client_id]['Available_Limit'].values[0],
                       "locale":"en_GB"}
    
def get_client_details():
     logging.info("Fetching all and custom client details")
     client_det  = api_call("GET",addto_queue_url,{"tenantIdentifier":"sokowatch","exportId":5,"fileFormat":"csv"})
     if client_det:
         response = api_call("GET",reportdet_url.format(str(client_det)))
         if response:
             client_data = pd.read_csv(response[1]['value'],delimiter=';')

     all_client_det = api_call("GET",clientdet_url)
     client_details = pd.json_normalize(all_client_det['pageItems'])
     client_details  = pd.merge(client_data,client_details[['id','externalId']],how='left',right_on='id',left_on='Client Id')
                 
     return client_details
    
def calculate_available_limit(loans_per_client,client_data):
    logging.info("Calculating Available Limit")

    client_limit = pd.merge(client_data[['Client Id','External Id','Client Details - Credit Limit', 'Client Details - Available Credit Limit',]],loans_per_client,how='left',right_on="clientId",left_on = 'Client Id')
    client_limit['Client Details - Credit Limit'] = client_limit['Client Details - Credit Limit'].astype(float).fillna(0)

    # Available Credit Limit = Credit Limit - Current Outstanding balance
    client_limit['AvailableCreditLimit'] = client_limit['Client Details - Credit Limit'] - client_limit['Outstanding_amount']
    
    # If a customer has outstanding balance then the available limit will be set to 0 else it will be reset to the actual credit limit
    client_limit['Available_Limit'] = client_limit.apply(lambda x:0 if x["Outstanding_amount"] > 0 else x['AvailableCreditLimit'],axis=1) 

    # Compare existing available limit in client data fetched from Musoni with the available limit calculated. If it is modified then set to True
    client_limit['is_limit_changed'] = np.where(client_limit['Client Details - Available Credit Limit'] == client_limit['Available_Limit'],False,True)
    # Fetch only client id's whose limit has changed.
    client_ids = list(client_limit['Client Id'][client_limit['is_limit_changed']==True].unique())
    logging.info("Client id's to update %s",str(client_ids))
    return client_limit,client_ids

def update_available_limit():
    
    logging.info("Fetching Loan details from Musoni")
    try:
        loan_data = api_call("GET",loandet_url)
        if loan_data:
                loan_data = pd.json_normalize(loan_data['pageItems'])
                loan_data['Outstanding_amount'] = loan_data["summary.totalOutstanding"] .apply(lambda x: 0 if x < 0  else x)
                loans_per_client = loan_data.groupby("clientId",as_index=False)["Outstanding_amount"].sum()
                
                client_data = get_client_details()
                
                if len(client_data)>=1 and len(loan_data)>=1:
                   client_limit,client_ids = calculate_available_limit(loans_per_client,client_data)
                   
                for client_id in client_ids:
                    try:
                        response = api_call("PUT",update_clientdet_url.format(str(client_id)),{"tenantIdentifier":"sokowatch","apptable":"m_client"},json = create_payload(client_id,client_limit))
                        logging.info(response)
                    except Exception as e:
                        logging.exception("Error - %s while updating client data for client id - %s",str(e),str(client_id ))
                return client_limit,client_ids

    except Exception as e:
        raise Exception("Error '{}' caught while processing func : update_available limit()".format(str(e) ))
        
def update_credit_limit_toIMS(client_limit,client_ids):
    logging.info("Inside func: update_credit_limit_toIMS() ")
    try:
        conn = psycopg2.connect(
                database= DB_CONNECTION_DB, user=DB_CONNECTION_USERNAME, 
                password= DB_CONNECTION_PASSWORD, host=DB_CONNECTION_HOST, 
                port= DB_CONNECTION_PORT
                                )
        cursor = conn.cursor()
        
        cursor.execute("CREATE TEMP TABLE musoni_ims_limit_update (customer_id varchar(70),musoni_client_id varchar(30),credit_limit float,available_limit float);")
        
        cursor.execute("CREATE INDEX customer_id_idx on musoni_ims_limit_update(customer_id);")
        
        temp = client_limit[["externalId","client_id","Available_Credi10","Available_Limit"]]
        
        temp = temp[temp['Client Id'].isin(client_ids)]
        temp['Client Id'] = temp['Client Id'].astype('str')
        
        sio = StringIO()
        writer = csv.writer(sio)
        writer.writerows(temp.values)
        sio.seek(0)
        
        with cursor as c:
            c.copy_from(
                file=sio,
                table="musoni_ims_limit_update",
                columns=[
                    "customer_id",
                    "musoni_client_id",
                    "credit_limit",
                    "available_limit"
        
                ],
                sep=","
            )
            conn.commit()
            
        start_time = datetime.datetime.now()
        update_sql = " UPDATE customers  SET credit_limit = t.available_limit FROM musoni_ims_limit_update t WHERE id = t.customer_id "   
        logging.info("Update customers table - Execution time %s",str(datetime.datetime.now() - start_time))
        
        cursor.execute(update_sql)
        conn.commit()
        
        conn.close()
    except Exception as e:
        logging.exception("Error '%s' caught while updating credit limit data to customers table",str(e) )
        raise Exception("Error {} while updating credit limit to customers table in IMS".format(str(e)))
        


if __name__=="__main__":
    try:
        client_limit,client_ids = update_available_limit()
        update_credit_limit_toIMS(client_limit,client_ids)   

    except Exception as e:
        error_log.append(str(e))
        
   
    if  len(error_log)>=1:
        logging.info("Summary : \n\n Script ran with the following errors -- %s",(" ".join(error_log)))
    else:
        logging.info("Script ran successfully")
